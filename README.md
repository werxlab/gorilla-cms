# Gorilla CMS

Gorilla CMS is a content management system built on top of the [Symfony Framework](https://symfony.com). Adding some of our glue. There is quite some consideration on integrating code (partial code ?) written by an AI.

## Visuals
Screenshots soon to come!

## Installation
Info soon to come!

## Usage
Info soon to come!

## Support
Support, bug reports, suggestions and more in our [ISSUES AREA](../../issues).

There will also ba a [forum](https://werxlab.org/forum) added shortly.

## Roadmap
Ideas for releases in the future, can be seen on our [MILESTONES](../milestones).

## Contributing
Info soon to come!

## License
This is an open source project. 
Licensed under [BSD 3-Clause "New" or "Revised" License](LICENSE),  [learn more](https://choosealicense.com/licenses/bsd-3-clause/) for details.

## Project status
This project is in active development. 
